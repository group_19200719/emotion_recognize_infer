# Описание

Репозиторий содержит скрипты для инференса моделей для распознавания эмоций на лицах людей.

## Треккинг эмоций на видео

Скрипт `make_emotion_tracking_on_video.py` осуществляет треккинг эмоция людей на видео из видеоконференций (`Zoom`, `Skype` и т.д.).

Скрипт сгенерирует видео демо с визуализацией распознанных лиц и распознанными эмоциями участников онлайн конференции.

(Поключающиеся и отключающиеся участники конференции учитываются).

![emotion_tracking_on_video](repo_pics/emotion_tracking_on_Zoom_RAVDESS.gif)

Скрипт сгенерирует `output_preds/face_detections.csv` который будет содержать по кадровую разметку видео.

Данная разметка удобна для использования в сторонних `API`.

Для каждого участника конференции будет созданы 3 столбца:

`person_{id}`, `person_{id}_emo`, `person_{id}_score`.

* Ряды представляют собой каждый кадр видео.
* `person_{id}` содержит bounding box лица персоны с поярковым номером `id`
* `person_{id}_emo` содержит текстовое описание эмоции, которую испытывала персона с поярковым номером `id` по мнению системы компьютерного зрения.
* `person_{id}_score` содержит значение вероятности того, что человек испытывает именно эту эмоцию.


![emotion_tracking_on_Zoom_RAVDESS](repo_pics/emotion_tracking_annot_on_video.jpg)

Однако из таблицы не ясно, кто является персоной с поярковым номером `id` и как она выглядит.
Поэтому скрипт сделает папку `output_preds/persons_faces` в которой будет по одному crop'у лица для каждого участника онлайн конференции в момент его появления:

```
output_preds/persons_faces
├── person_1.jpg
├── person_2.jpg
├── person_3.jpg
├── person_4.jpg
├── person_5.jpg
└── person_6.jpg
```
`person_1.jpg`

![person_1.jpg](output_preds/persons_faces/person_1.jpg)

`person_2.jpg`

![person_2.jpg](output_preds/persons_faces/person_2.jpg)

`person_3.jpg`

![person_3.jpg](output_preds/persons_faces/person_3.jpg)

`person_4.jpg`

![person_4.jpg](output_preds/persons_faces/person_4.jpg)

`person_5.jpg`

![person_5.jpg](output_preds/persons_faces/person_5.jpg)

`person_6.jpg`

![person_6.jpg](output_preds/persons_faces/person_6.jpg)

Работает и в случае запоздалого входа и раннего выхода пользователя из онлайн конференции:

![emotion_tracking_on_video](repo_pics/emotion_tracking_on_video.gif)
repo_pics/emotion_tracking_on_video.gif
