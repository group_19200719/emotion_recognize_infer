from emotion_det_net_infer import FaceDetector

if __name__ == "__main__":
    # отдаем картинку с 4 лицами актрисы №24 из датасета RAVDESS
    image_path = "test_pics/frame_0.jpg"
    
    # filename для фото с визуализацией детекций
    output_img_name = "test_face_detections.jpg"

    # инициализируем детектор лиц RetinaFace
    facedet = FaceDetector()
    
    # загружаем картинку
    image = facedet.load_image(image_path)
    
    # детектируем все лица на картинке
    face_detections = facedet.detect_faces(image)
    
    # визуализируем bounding boxes для всех найденных лиц
    image = facedet.viz_detections(image, face_detections)
    
    # сохраняем изображение с детекцией лиц
    image.save(output_img_name)
