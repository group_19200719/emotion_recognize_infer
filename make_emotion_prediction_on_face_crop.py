from dotenv import find_dotenv, load_dotenv
from emotion_net_infer import EmotionSeqClassifier


def read_env_variables():
    load_dotenv(find_dotenv())


if __name__ == "__main__":
    # считаем переменные в рабочее окружение
    read_env_variables()

    # отдаем картинку с 4 лицами актрисы №24 из датасета RAVDESS
    image_path = "test_pics/02-01-02-01-01-01-24_frame_30_0.png"

    # инициализируем классификатор эмоций для последовательности
    # из вырезанных с изображения лиц
    emo_seq_classifier = EmotionSeqClassifier()

    # загружаем картинку
    pil_image = emo_seq_classifier.load_image(image_path)

    # предсказываем ее эмоцию
    emotion_class, score = emo_seq_classifier.predict_on_image(
        pil_image, return_label=True
    )

    print(emotion_class, score)
